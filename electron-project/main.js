const { app, BrowserWindow } = require('electron');
const path = require('path');

function createWindow() {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false, // nécessaire pour Flume et React
      enableRemoteModule: true, // selon les versions d'Electron
    }
  });

  // Charge le fichier HTML transpile dans le répertoire "public"
  win.loadFile(path.join(__dirname, 'public', 'index.html'));

}

app.whenReady().then(() => {
  createWindow();

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
