"use strict";

var _react = _interopRequireDefault(require("react"));
var _reactDom = _interopRequireDefault(require("react-dom"));
var _flume = require("flume");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Créer une nouvelle instance de FlumeConfig
var config = new _flume.FlumeConfig();

// Définir les types de ports
config.addPortType({
  type: "text",
  name: "text",
  label: "Text",
  controls: [{
    type: "text",
    name: "string",
    label: "Text"
  }]
});

// Définir le type de nœud
config.addNodeType({
  type: "displayText",
  label: "Display Text",
  description: "Displays text",
  inputs: function inputs(ports) {
    return [ports.text()];
  },
  outputs: function outputs(ports) {
    return [];
  }
});

// Composant App qui affiche l'éditeur de nodes Flume
function App() {
  return /*#__PURE__*/_react["default"].createElement("div", {
    style: {
      height: '100vh',
      width: '100vw'
    }
  }, " ", /*#__PURE__*/_react["default"].createElement(_flume.NodeEditor, {
    portTypes: config.portTypes,
    nodeTypes: config.nodeTypes,
    onChange: function onChange(nodes) {
      return console.log(nodes);
    } // Log des nodes dans la console
  }));
}

// Rendu de l'application React dans le div avec l'ID root
_reactDom["default"].render(/*#__PURE__*/_react["default"].createElement(App, null), document.getElementById('root'));