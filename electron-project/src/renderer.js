import React from 'react';
import ReactDOM from 'react-dom';
import { FlumeConfig, NodeEditor } from 'flume';

// Créer une nouvelle instance de FlumeConfig
const config = new FlumeConfig();

// Définir les types de ports
config.addPortType({
  type: "text",
  name: "text",
  label: "Text",
  controls: [
    {
      type: "text",
      name: "string",
      label: "Text"
    }
  ]
});

// Définir le type de nœud
config.addNodeType({
  type: "displayText",
  label: "Display Text",
  description: "Displays text",
  inputs: (ports) => [ports.text()],
  outputs: (ports) => []
});

// Composant App qui affiche l'éditeur de nodes Flume
function App() {
  return (
    <div style={{ height: '100vh', width: '100vw' }}> {/* Prend tout l'espace de la fenêtre */}
      <NodeEditor
        portTypes={config.portTypes}
        nodeTypes={config.nodeTypes}
        onChange={(nodes) => console.log(nodes)} // Log des nodes dans la console
      />
    </div>
  );
}

// Rendu de l'application React dans le div avec l'ID root
ReactDOM.render(<App />, document.getElementById('root'));