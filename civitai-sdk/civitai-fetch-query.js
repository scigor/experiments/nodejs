// Importer le SDK Civitai
const { Civitai } = require('civitai');

// Créer une instance de Civitai avec votre token d'authentification
const civitai = new Civitai({
  auth: "Bearer 50b66f484353442e57b908149f4a822f", // Remplacez par votre propre token
});

//const civitai = new Civitai({
//  auth: process.env.CIVITAI_API_TOKEN, // Assurez-vous que cette variable est définie
//});

// Configuration de la requête
const query = {
  properties: {
    userId: 5611225, 
  },
};

const detailed = false;

// Fonction principale pour exécuter la requête
async function fetchJobs() {
  try {
    // Effectuer la requête pour obtenir les jobs
    //const response = await civitai.jobs.getByQuery(query, detailed);
    const response = await civitai.jobs.getByToken('eyJKb2JzIjpbIjkxOTFlMzI5LWFkNzAtNDJmMi04ZDMxLTg2NDdlOGY3N2Q4MiJdfQ==');
    //.const response = await civitai.jobs.getById("9191e329-ad70-42f2-8d31-8647e8f77d82");
    console.log("Résultats obtenus :", response);
  } catch (error) {
    console.error("Une erreur est survenue :", error);
  }
}

// Appeler la fonction pour lancer la requête
fetchJobs();

