import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect  } from 'react';
import FormRendu from './FormRendu';
//import { models, loras } from './data';
import axios from 'axios';

const Form = ({ posPrompt, setPosPrompt, slidesData, setSlidesData }) => {
    const [jsonModels, setJsonModels] = useState([]);
    const [filteredModels, setFilteredModels] = useState([]);
    const [filteredLoras, setFilteredLoras] = useState([]);
    const [selectedModel, setSelectedModel] = useState('');
    const [selectedLora, setSelectedLora] = useState('');

    //const [posPrompt, setPosPrompt] = useState('');
    const [negPrompt, setNegPrompt] = useState('');
    const [selectedCheckboxes, setSelectedCheckboxes] = useState({
        simpleBackground: false,
        fullBody: false,
    });

    useEffect(() => {
        const fetchData = async () => {
            try {
              /*
              // On filtre ici uniquement les modèles "checkpoint" et on met à jour l'état
              const checkpointModels = models.filter(model => model.includes('checkpoint'));
              setFilteredModels(checkpointModels);
              */
              const response = await axios.get('http://212.47.250.95:5000/api/civitai');
              console.log(response.data);

              const jsonModels = response.data.urn;
	      setJsonModels(jsonModels);

              const models = jsonModels.filter(model => model.includes('checkpoint'));
              setFilteredModels(models);

              const model = models[0];
	      setSelectedModel(model);

              // Extraire le "modelType" du premier modèle (par exemple, "sdxl" ou "sd1")
              const modelType = model.split(':')[2]; // Récupère le 3e élément (ex: "sdxl")
              console.log("First Model Type:", modelType);

              // Filtrer les loras correspondant au premier modèle
              //const filtered = loras.filter(lora => lora.includes(modelType));
	      const loras = models.filter(lora => lora.includes('lora') && lora.includes(modelType));
              setFilteredLoras(loras);
              console.log("Filtered Loras:", loras);

              const lora = loras[0];
	      setSelectedLora(lora);
            } catch (error) {
              console.error('Error submitting data:', error);
            }
         };

        fetchData(); 
    }, []); // Le tableau vide comme dépendance signifie que ça ne s'exécutera qu'une fois au montage

    // Effet pour mettre à jour la liste des loras chaque fois que le modèle sélectionné change
    useEffect(() => {
        if (selectedModel) {
            // Extraire la partie du modèle qui est pertinente pour le filtrage des loras
            const modelType = selectedModel.split(':')[2];
            console.log("Model Type:", modelType);

            // Filtrer les loras en fonction du type de modèle sélectionné
            const loras = jsonModels.filter((lora) => lora.includes('lora') && lora.includes(modelType));
            console.log("Filtered Loras:", loras);
            setFilteredLoras(loras);

            const lora = loras[0]; // Mettre à jour le lora sélectionné
            setSelectedLora(lora);
        }
    }, [selectedModel, jsonModels]);

    useEffect(() => {
      // Mettre à jour posPrompt avec le positive_prompt de la première image si disponible
      if (slidesData.length > 0) {
        setPosPrompt(slidesData[0].positive_prompt || '');
      }
      console.log("Slides data mise à jour:", slidesData); 
    }, [slidesData, setPosPrompt]); // Déclenché chaque fois que slidesData est mis à jour

    const handleModelChange = async (e) => {
      const newModel = e.target.value;
      setSelectedModel(newModel); 
      console.log("selectedModel Model:", newModel);

      try {
	const modelId = e.target.value.split(':')[5].split('@')[0];

        const response = await fetch('http://212.47.250.95:5000/api/images', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ modelId }),
        });
	if (!response.ok) {
          throw new Error('Erreur lors de la récupération des images');
        }
        const data = await response.json();
	console.log("API Response:", data); // Log de la réponse de l'API
        if (data) {
          setSlidesData(data);  // Mettre à jour les données d'images
        }
      } catch (error) {
        console.error('Erreur lors de la récupération des images:', error);
      }
    };
        
    const handleLoraChange = (e) => {
        const newLora = e.target.value;
	setSelectedLora(newLora); 
        console.log("selectedLora Lora:", newLora);
    };        

    const handleCheckboxChange = (event) => {
        setSelectedCheckboxes({
            ...selectedCheckboxes,
            [event.target.name]: event.target.checked,
        });
    };
 
    const handlePosChange = (e) => {
        setPosPrompt(e.target.value);
    };
 
    const handleNegChange = (e) => {
        setNegPrompt(e.target.value);
    };
/*
    const output = `{"posPrompt":"${posPrompt}" ",negPrompt:"${negPrompt}" ${Object.keys(selectedCheckboxes)
        .filter(key => selectedCheckboxes[key])
        .join(' ')}"}"`;
*/
    const output = JSON.stringify({
        model: selectedModel,
	lora: selectedLora,
        posPrompt: posPrompt,
        negPrompt: negPrompt,
        checkboxes: Object.keys(selectedCheckboxes)
            .filter(key => selectedCheckboxes[key])
    }, null, 2);

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post('http://212.47.250.95:5000/api/civitai_create', 
		    { text: output }, 
		    { headers: { 'Content-Type': 'application/json' } });
            console.log(response.data);
        } catch (error) {
            console.error('Error submitting data:', error);
        }
    };	

    return (
        <div className="App">
            <FormRendu 
                filteredModels={filteredModels}
                filteredLoras={filteredLoras}
                handleModelChange={handleModelChange}
                handleLoraChange={handleLoraChange}
	        selectedModel={selectedModel}
	        selectedLora={selectedLora}
                posPrompt={posPrompt}
                negPrompt={negPrompt}
                onPosChange={handlePosChange}
                onNegChange={handleNegChange}
                output={output}
	        handleSubmit={handleSubmit}
            />
        </div>
    );
}

export default Form;
