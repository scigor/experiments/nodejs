// Civitai.js
import logo from './logo.svg';
import './App.css';
import React, { useState } from 'react';
import Form from './Form';
import ImageSlider from './ImageSlider';

const Civitai = () => {
  // Etat pour les sliders
  const [slidesData, setSlidesData] = useState([]);

  // État pour la description
  const [posPrompt, setPosPrompt] = useState('');

  // Fonction pour copier (ss evt) le prompt du slider vers le formulaire
  const handleCpyPosPromptChange = (posPrompt) => {
      setPosPrompt(posPrompt);
  };

  return (
    <div className="container">
      {/* Colonne Formulaire */}
      <div className="column form-column">
        <h2>Formulaire Civitai</h2>
        <Form 
	  posPrompt={posPrompt}
	  setPosPrompt={setPosPrompt} 
	  slidesData={slidesData}
	  setSlidesData={setSlidesData} />
      </div>

      {/* Colonne Slider d'Images */}
      <div className="column">
        <h2>Slider d'Images</h2>
        <ImageSlider 
	  slides={slidesData}
	  handleCpyPosPromptChange={handleCpyPosPromptChange} />
      </div>
    </div>
  );
};

export default Civitai;
