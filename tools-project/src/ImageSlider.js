// ImageSlider.js
import './App.css';
import React, { useState, useEffect  } from 'react';
import ImageSliderRendu from './ImageSliderRendu';
import axios from 'axios';

const ImageSlider = ({ slides, handleCpyPosPromptChange }) => {
  const [currentIndex, setCurrentIndex] = useState(0);

  const goToNext = () => {
    setCurrentIndex((currentIndex + 1) % slides.length);
  };

  const goToPrevious = () => {
    setCurrentIndex((currentIndex - 1 + slides.length) % slides.length);
  };

  return (
        <div className="App">
            <ImageSliderRendu
                goToPrevious={goToPrevious}
	        goToNext={goToNext}
                slides={slides}
	        currentIndex={currentIndex}
	        handleCpyPosPromptChange={handleCpyPosPromptChange}
	    />
        </div>
  );
};

export default ImageSlider;
