import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { FlumeConfig, NodeEditor } from 'flume';
/*
import ActorAnimationSelector from "./ActorAnimationSelector"; 
import ActorSelector from "./ActorSelector"; 
*/
// Créer une nouvelle instance de FlumeConfig
/*
const config = new FlumeConfig();

// Définir le type de port
config.addPortType({
  type: "text",
  name: "text",
  label: "Text",
  controls: [
    {
      type: "text",
      name: "string",
      label: "Text"
    }
  ]
});

// Définir le type de nœud
config.addNodeType({
  type: "displayText",
  label: "Display Text",
  description: "Displays text",
  inputs: (ports) => [ports.text()],
  outputs: (ports) => []
});

// Définir le type de nœud
config.addPortType({
  type: "actor",
  name: "actor",
  label: "Actor",
  controls: [
    {
      type: "select",
      name: "actor",
      label: "Select Actor",
      options: [
        { value: 'actor1', label: 'Actor 1' },
        { value: 'actor2', label: 'Actor 2' },
        { value: 'actor3', label: 'Actor 3' },
      ],
    },
  ],
});

// Définir le type de nœud
config.addNodeType({
  type: "simpleNode",
  label: "Simple Node",
  description: "A simple node for testing",
  inputs: (ports) => [ports.actor()],
  outputs: (ports) => [ports.actor()]
});

// Définir le type de port
config.addPortType({
  type: "animation",
  name: "animation",
  label: "Animation",
  controls: [
    {
      type: "select",
      name: "animation",
      label: "Select Animation",
    },
  ],
});

// Définir le type de nœud

config.addNodeType({
  type: "actorAnimationSelector",
  label: "Actor Animation Selector",
  description: "Sélectionner un acteur et une animation",
  inputs: (ports) => [ports.actor()],
  outputs: (ports) => [ports.animation()],
});

*/

// Ajouter un type de nœud pour le sélecteur d'acteur et d'animation
/*config.addNodeType({
  type: "actorAnimationSelector", // Assurez-vous que le type est bien défini ici
  label: "Actor & Animation Selector",
  description: "Select an actor and choose an animation for the actor",
  inputs: (ports) => [ports.actor()],
  outputs: (ports) => [ports.animation()],
  controls: [
    {
      type: "custom", // Contrôle personnalisé
      name: "actorAnimationControl",
      label: "Actor and Animation Selector",
      render: ({ value, onChange }) => (
        <ActorAnimationSelector value={value} onChange={onChange} />
      ),
    },
  ],
});*/


// Composant principal Flume avec NodeEditor
const Flume = () => {
  const [logs, setLogs] = useState([]);
  const [actors, setActors] = useState([]);
  const [animations, setAnimations] = useState([]);
  const [selectedActor, setSelectedActor] = useState(null);
  const [flumeConfig, setFlumeConfig] = useState(new FlumeConfig());

  const logToState = (message, setLogs) => {
    setLogs((prevLogs) => {
      const updatedLogs = [...prevLogs, message];
      localStorage.setItem("logs", JSON.stringify(updatedLogs)); // Sauvegarde dans localStorage
      console.log("Logs mis à jour:", updatedLogs);  // Vérifie que les logs sont bien mis à jour
      return updatedLogs;
    });
  };
  
  const saveLogsToFile = (logs) => {
    const blob = new Blob([logs.join("\n")], { type: "text/plain" });
    const link = document.createElement("a");
    link.href = URL.createObjectURL(blob);
    link.download = "logs.txt";
    link.click();
  };

  // Fonction qui met à jour les animations en fonction de l'acteur sélectionné
  /*const updateAnimations = async (actor) => {
    if (!actor) return;

    try {
      const response = await fetch(`http://localhost:3001/api/states/${actor}`);
      if (!response.ok) throw new Error(`Erreur API animations: ${response.status}`);
      const data = await response.json();

      const animationOptions = data.map((anim) => ({
        value: anim,
        label: anim,
      }));

      setAnimations(animationOptions);  // Met à jour les animations
    } catch (error) {
      console.error("Erreur lors du chargement des animations :", error);
    }
  };*/

  // Charger les acteurs à partir de l'API

  useEffect(() => {
    const fetchActors = async () => {
      try {
        const response = await fetch("http://localhost:3001/api/actors");
        if (!response.ok) throw new Error(`Erreur API acteurs: ${response.status}`);
        const data = await response.json();

        const actorOptions = data.map(actor => ({
          value: actor,
          label: actor
        }));

        setActors(actorOptions);
        logToState("✅ fetchActors:"+JSON.stringify(actorOptions, null, 2), setLogs);
      } catch (error) {
        logToState(`❌ Erreur lors de la récupération des actors :${error}`, setLogs);
      }
    };

    fetchActors();
  }, []);

  // Charger les animations en fonction de l'acteur sélectionné
  useEffect(() => {
    const fetchAnimations = async () => {
      logToState("✅ fetchAnimations:"+JSON.stringify(selectedActor, null, 2), setLogs);
      if (!selectedActor) return;  // Si aucun acteur n'est sélectionné, rien à faire
      try {
        const response = await fetch(`http://localhost:3001/api/states/${selectedActor}`);
        if (!response.ok) throw new Error(`Erreur API animations: ${response.status}`);
        const data = await response.json();

        const animationOptions = data.map((anim) => ({
          value: anim,
          label: anim,
        }));
        setAnimations(animationOptions);  // Mettre à jour la liste des animations
      } catch (error) {
        logToState(`❌ Erreur lors de la récupération des animations :${error}`, setLogs);
      }
    };

    fetchAnimations();
  }, [selectedActor]);  // Relancer la récupération d'animations lorsque l'acteur change
  
  // 3️⃣ Mise à jour de FlumeConfig quand acteurs ou animations changent
  useEffect(() => {
    const newConfig = new FlumeConfig();

    // Port Acteur
    newConfig.addPortType({
      type: "actor",
      name: "actor",
      label: "Actor",
      controls: [
        {
          type: "select",
          name: "actor",
          label: "Select Actor",
          options: actors,
        },
      ],
    });

    // Port Animation (mise à jour dynamique)
    newConfig.addPortType({
      type: "animation",
      name: "animation",
      label: "Animation",
      controls: [
        {
          type: "select",
          name: "animation",
          label: "Select Animation",
          options: animations, // ⚡ Mise à jour automatique après sélection
        },
      ],
    });

    // Définition du nœud
    newConfig.addNodeType({
      type: "actorAnimationSelector",
      label: "Actor Animation Selector",
      description: "Sélectionner un acteur et une animation",
      inputs: (ports) => [ports.actor({ hidePort: true }), ports.animation({ hidePort: true })],
      outputs: (ports) => [ports.actor(), ports.animation()],
    });

    /*newConfig.onPortChange = (port, value) => {
      //if (port.type === "actor") {
        console.log("Acteur sélectionné :", value);
        logToState(`Acteur sélectionné : ${value}`, setLogs);
        setSelectedActor(value);
      //}
      //if (port.type === "animation") {
        console.log("Animation sélectionnée :", value);
        logToState(`Animation sélectionnée : ${value}`, setLogs);
      //}
    };*/

    // Forcer Flume à se rafraîchir
    setFlumeConfig(newConfig);
  }, [actors, animations]); // ⚡ Mise à jour uniquement après changement de données

  const handleActorChange = (event) => {
    const actor = event.target.value;
    logToState(`🚀 handleActorChange:Acteur sélectionné : ${actor}`, setLogs);
    setSelectedActor(actor);    
  };
  
  return (
    <div>
      <h1>Flume Editor avec Logs</h1>
      <div style={{ height: "100vh", width: "100vw" }}>
      <NodeEditor
        portTypes={flumeConfig.portTypes}
        nodeTypes={flumeConfig.nodeTypes}
        onChange={(nodes) => {
          
          //logToState(JSON.stringify(nodes, null, 2),setLogs);

          // Chercher la valeur sélectionnée dans le port actor
          Object.values(nodes).forEach((node) => {
            
            Object.entries(node.inputData).forEach(([portName, port]) => {              
              if (portName === "actor" && port.actor) {
                logToState(`📌 Port détecté : ${portName} ${port.actor}`,setLogs);
                setSelectedActor(port.actor);
              }
            });
          });
        }}
      />
    </div>
      <div>
        <button onClick={() => logToState('Log Message Added', setLogs)}>Log Message</button>
      </div>    
      <div>
        <button onClick={() => saveLogsToFile(logs)}>Télécharger les logs</button>
      </div>
      {/*
      <div>
        <label>Choisir un acteur :</label>
        <select onChange={handleActorChange} value={selectedActor}>
        <option value="">Sélectionner un acteur</option>
          {actors.map(actor => (
            <option key={actor.label} value={actor.value}>
              {actor.label}
            </option>
          ))}
        </select>
      </div>*/}

      <div>
        <h2>Logs :</h2>
        <ul>
          {logs.map((log, index) => (
            <li key={index}>{log}</li>
          ))}
        </ul>        
      </div>

    </div>
  );
};

export default Flume;