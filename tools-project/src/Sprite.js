import React, { useState, useEffect, useRef } from "react";
import ActorSelect from "./Sprite/ActorSelect";
import StateSelect from "./Sprite/StateSelect";
import FrameDisplay from "./Sprite/FrameDisplay";
import ActorControls from "./Sprite/ActorControls"; // Renommé
import FrameList from "./Sprite/FrameList";
import FrameControls from "./Sprite/FrameControls"; // Nouveau
import "./Sprite/Sprite.css";

function App() {
  const [actors, setActors] = useState([]);
  const [selectedActor, setSelectedActor] = useState("");
  const [states, setStates] = useState([]);
  const [selectedState, setSelectedState] = useState("");
  const [frames, setFrames] = useState([]);
  const [currentFrameIndex, setCurrentFrameIndex] = useState(0);
  const [frameProperties, setFrameProperties] = useState({ x: 0, y: 0 });

  const timerRef = useRef(null);

  useEffect(() => {
    fetch("http://localhost:3001/api/actors")
      .then((response) => response.json())
      .then((data) => setActors(data))
      .catch((err) => console.error("Error fetching actors:", err));
  }, []);

  useEffect(() => {
    if (selectedActor) {
      fetch(`http://localhost:3001/api/states/${selectedActor}`)
        .then((response) => response.json())
        .then((data) => setStates(data))
        .catch((err) => console.error("Error fetching states:", err));
    }
  }, [selectedActor]);

  useEffect(() => {
    if (selectedActor && selectedState) {
      fetch(`http://localhost:3001/api/frames/${selectedActor}/${selectedState}`)
        .then((response) => response.json())
        .then((data) => {
          const framesWithOffsets = data.map((frame) => ({
            ...frame,
            x: frame.x || 0,
            y: frame.y || 0,
          }));
          setFrames(framesWithOffsets);
          setCurrentFrameIndex(0); // Réinitialiser la position de la liste à 0
          setFrameProperties({
            x: framesWithOffsets[0]?.x || 0,
            y: framesWithOffsets[0]?.y || 0,
          });
        })
        .catch((err) => console.error("Error fetching frames:", err));
    }
  }, [selectedActor, selectedState]);

  useEffect(() => {
    // Redémarrer l'animation lorsque le nombre de frames change
    if (frames.length > 0) {
      restartAnimation();
    }
  }, [frames.length]);
  
  const restartAnimation = () => {
    stopAnimation(); // Stopper toute animation en cours
    startAnimation(); // Redémarrer avec les nouvelles frames
  };

  
  const startAnimation = () => {
    if (timerRef.current) return;
    timerRef.current = setInterval(() => {
      setCurrentFrameIndex((prevIndex) => (prevIndex + 1) % frames.length);
    }, 100);
  };

  const stopAnimation = () => {
    if (timerRef.current) {
      clearInterval(timerRef.current);
      timerRef.current = null;
    }
  };

  const updateFrameProperties = (updates) => {
    setFrames((prevFrames) =>
      prevFrames.map((frame, index) =>
        index === currentFrameIndex ? { ...frame, ...updates } : frame
      )
    );
  };

  const saveProperties = () => {
    console.log("Saved properties:", frames);
  };

  return (
    <div className="App">
      <h1>Animator App</h1>

      <div className="main-container">
        {/* Section gauche : Animation */}
        <div className="animation-section">
          {frames.length > 0 ? (
            <FrameDisplay
              frame={frames[currentFrameIndex]}
              selectedActor={selectedActor}
              selectedState={selectedState}
            />
          ) : (
            "No frames to display"
          )}
        </div>

        {/* Section droite : Contrôles et Propriétés */}
        <div className="right-section">
          <div className="top-right">
            {/* Sélecteurs Actor et State */}
            <div className="selector-container">
              <ActorSelect
                actors={actors}
                selectedActor={selectedActor}
                setSelectedActor={setSelectedActor}
              />
              <StateSelect
                states={states}
                selectedState={selectedState}
                setSelectedState={setSelectedState}
              />
            </div>
            <ActorControls
              startAnimation={startAnimation}
              stopAnimation={stopAnimation}
            />
          </div>

          <div className="bottom-right">
            {/* Liste des Frames */}
            <FrameList
              frames={frames}
              currentFrameIndex={currentFrameIndex}
              setCurrentFrameIndex={setCurrentFrameIndex}
              setFrameProperties={setFrameProperties}
            />

            {/* Propriétés du Frame */}
            <FrameControls
              frameProperties={frameProperties}
              setFrameProperties={setFrameProperties}
              updateFrameProperties={updateFrameProperties}
              saveProperties={saveProperties}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
