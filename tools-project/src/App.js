/*
 * import logo from './logo.svg';
 * import './App.css';
*/
import React, { useState } from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import Civitai from './Civitai';
import Flume from './Flume';
import Sprite from './Sprite';

function App() {
    return (
	<Router>
	  <div>
	    <nav>
	      <ul>
		<li>
		  <Link to="/">Civitai</Link>
		</li>
		<li>
		  <Link to="/flume">Flume</Link>
		</li>
		<li>
		  <Link to="/sprite">Sprite</Link>
		</li>		
	      </ul>
	    </nav>
	    <Routes>
	      <Route path="/" element={<Civitai />} />
	      <Route path="/flume" element={<Flume />} />
		  <Route path="/sprite" element={<Sprite />} />
	    </Routes>
	  </div>
	</Router>
    );
}

export default App;
