// src/data.js
export const models = [
    "urn:air:sdxl:checkpoint:civitai:822320@919559",
    "urn:air:sd1.5:checkpoint:civitai:8421320@91914",
    // Ajoutez d'autres modèles ici
];

export const loras = [
    "urn:air:sdxl:lora:civitai:230035@259629",
    "urn:air:sd1:lora:civitai:44960@52870",
    // Ajoutez d'autres loras ici
];
