import React from "react";

function FrameDisplay({ frame, selectedActor, selectedState }) {
  return (
    <div className="frame-display">
      <div className="cross-hair">
        {/* Axe horizontal */}
        <div className="axis horizontal"></div>
        {/* Axe vertical */}
        <div className="axis vertical"></div>
      </div>
  
      {/* Frame de l'animation */}
      <div className="frame" style={{
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: `translate(-50%, -50%) translate(${frame.x}px, ${-frame.y}px)`
      }}>
        <img
          src={`http://localhost:3001/api/frames/${selectedActor}/${selectedState}/${frame.file}`}
          alt={frame.file}
        />
      </div>
    </div>
  );
}

export default FrameDisplay;
