import React from "react";

function ActorSelect({ actors, selectedActor, setSelectedActor }) {
  return (
    <div>
      <label>Actor:</label>
      <select onChange={(e) => setSelectedActor(e.target.value)} value={selectedActor}>
        <option value="">Select an Actor</option>
        {actors.map((actor) => (
          <option key={actor} value={actor}>
            {actor}
          </option>
        ))}
      </select>
    </div>
  );
}

export default ActorSelect;
