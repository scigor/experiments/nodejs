import React from "react";

function ActorControls({ startAnimation, stopAnimation }) {
  return (
    <div>
      <button onClick={startAnimation}>Play</button>
      <button onClick={stopAnimation}>Stop</button>
    </div>
  );
}

export default ActorControls;
