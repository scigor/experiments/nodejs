from PIL import Image
import os
import shutil

def detect_best_crop_size(images, multiple):
    """
    Détecte la meilleure taille (multiple de 8) pour toutes les images d'un dossier.

    Args:
        images: Liste des images PIL à analyser.
        multiple: Le multiple auquel ajuster les dimensions.

    Returns:
        Tuple (width, height) de la meilleure taille trouvée.
    """
    max_width, max_height = 0, 0

    for image in images:
        pixels = image.load()
        width, height = image.size

        # Initialisation des limites
        top, left = height, width
        bottom, right = 0, 0

        # Détection des pixels non noirs/transparents
        for y in range(height):
            for x in range(width):
                r, g, b, a = pixels[x, y]
                if a > 0 and (r != 0 or g != 0 or b != 0):
                    top = min(top, y)
                    bottom = max(bottom, y)
                    left = min(left, x)
                    right = max(right, x)

        # Calcul des dimensions recadrées actuelles
        cropped_width = right - left + 1
        cropped_height = bottom - top + 1

        # Ajustement au multiple
        adjusted_width = ((cropped_width + multiple - 1) // multiple) * multiple
        adjusted_height = ((cropped_height + multiple - 1) // multiple) * multiple

        max_width = max(max_width, adjusted_width)
        max_height = max(max_height, adjusted_height)

    return max_width, max_height

def crop_image_to_fixed_size(image, fixed_width, fixed_height):
    """
    Recadre une image pour une taille fixe tout en conservant son contenu centré.

    Args:
        image: Image PIL à recadrer.
        fixed_width: Largeur fixe.
        fixed_height: Hauteur fixe.

    Returns:
        Image recadrée et ajustée.
    """
    image = image.convert("RGBA")
    pixels = image.load()
    width, height = image.size

    # Initialisation des limites
    top, left = height, width
    bottom, right = 0, 0

    # Détection des pixels non noirs/transparents
    for y in range(height):
        for x in range(width):
            r, g, b, a = pixels[x, y]
            if a > 0 and (r != 0 or g != 0 or b != 0):
                top = min(top, y)
                bottom = max(bottom, y)
                left = min(left, x)
                right = max(right, x)

    # Si aucune zone significative n'est trouvée, retourne une image vide
    if top >= bottom or left >= right:
        return Image.new("RGBA", (fixed_width, fixed_height), (0, 0, 0, 0))

    # Crop l'image et recentre-la
    cropped_image = image.crop((left, top, right + 1, bottom + 1))
    x_margin = (fixed_width - cropped_image.width) // 2
    y_margin = (fixed_height - cropped_image.height) // 2

    adjusted_image = Image.new("RGBA", (fixed_width, fixed_height), (0, 0, 0, 0))
    adjusted_image.paste(cropped_image, (x_margin, y_margin))

    return adjusted_image

def prepare_images(input_folders, tmp_folder, use_tmp, crop, uniform_size, multiple=8):
    """
    Prépare les images : les recadre et les sauvegarde dans un dossier temporaire si nécessaire.

    Args:
        input_folders: Liste des répertoires d'entrée contenant les images.
        tmp_folder: Répertoire temporaire pour stocker les images traitées.
        use_tmp: Booléen pour spécifier si le dossier temporaire doit être utilisé.
        crop: Booléen pour spécifier si les images doivent être recadrées.
        uniform_size: Booléen pour uniformiser la taille des images d'un dossier.
        multiple: Multiple auquel ajuster les dimensions (par défaut 8).

    Returns:
        Liste des dossiers contenant les images préparées.
    """
    prepared_folders = []

    if use_tmp:
        # Supprime et recrée le dossier temporaire
        if os.path.exists(tmp_folder):
            shutil.rmtree(tmp_folder)
        os.makedirs(tmp_folder)

    for folder in input_folders:
        folder_name = os.path.basename(folder)
        if use_tmp:
            tmp_subfolder = os.path.join(tmp_folder, folder_name)
            os.makedirs(tmp_subfolder)
        else:
            tmp_subfolder = folder  # Pas de tmp, on travaille directement sur les dossiers d'entrée

        print(f"\nTraitement du dossier : {folder_name}")

        images = []
        for filename in sorted(os.listdir(folder)):
            if filename.lower().endswith(('.png', '.jpg', '.jpeg')):
                input_path = os.path.join(folder, filename)
                images.append(Image.open(input_path).convert("RGBA"))

        # Détection de la meilleure taille pour ce dossier
        fixed_width, fixed_height = None, None
        if uniform_size and crop:
            fixed_width, fixed_height = detect_best_crop_size(images, multiple)
            print(f"Taille uniforme détectée : {fixed_width}x{fixed_height}")

        for image, filename in zip(images, sorted(os.listdir(folder))):
            output_path = os.path.join(tmp_subfolder, filename)
            if crop:
                if uniform_size:
                    image = crop_image_to_fixed_size(image, fixed_width, fixed_height)
                else:
                    image = crop_image_to_fixed_size(image, *detect_best_crop_size([image], multiple))
            if use_tmp:
                image.save(output_path)

        prepared_folders.append(tmp_subfolder)

    return prepared_folders

def create_spritesheet(prepared_folders, output_path, max_width=256):
    """
    Crée un spritesheet à partir des images préparées.

    Args:
        prepared_folders: Liste des dossiers contenant les images préparées.
        output_path: Chemin où sauvegarder le spritesheet final.
        max_width: Largeur maximale d'une ligne du spritesheet.
    """
    all_images = []
    total_width = 0  # Largeur maximale du spritesheet
    total_height = 0  # Hauteur totale du spritesheet

    # Parcourt les dossiers et charge les images
    for folder in prepared_folders:
        images = []
        for filename in sorted(os.listdir(folder)):
            if filename.lower().endswith(('.png', '.jpg', '.jpeg')):
                image_path = os.path.join(folder, filename)
                image = Image.open(image_path).convert("RGBA")
                images.append(image)

        if images:
            # Ajouter les images de cette ligne au spritesheet
            all_images.append(images)
            total_width = max(total_width, sum(img.width for img in images))
            total_height += max(img.height for img in images)

    # Création du spritesheet
    spritesheet = Image.new("RGBA", (total_width, total_height))

    # Placement des images dans le spritesheet
    y_offset = 0
    for images in all_images:
        x_offset = 0
        line_height = max(img.height for img in images)
        for img in images:
            spritesheet.paste(img, (x_offset, y_offset))
            x_offset += img.width
        y_offset += line_height

    # Sauvegarde du spritesheet final
    spritesheet.save(output_path + ".png")
    print(f"Spritesheet généré et sauvegardé sous : {output_path}.png")

    # Conversion de l'image en 8 bits (mode P) pour la sauvegarder en TGA sans compression
    spritesheet_8bit = spritesheet.convert("P", palette=Image.ADAPTIVE, colors=256)

    # Sauvegarde en format TGA sans compression (compression=None)
    spritesheet_8bit.save(output_path + ".tga", format="TGA", compression=None)
    print(f"Spritesheet généré et sauvegardé sous : {output_path}.tga")

# Exemples d'appel de la fonction
input_folders = [
    "PixelPrototypePlayer/sprites/idle",  # Contient 7 images de 16x16
    "PixelPrototypePlayer/sprites/walk"   # Contient 8 images de 16x16
]

tmp_folder = "tmp"  # Dossier temporaire
output_path = "PLAYER8"  # Chemin du spritesheet final

use_tmp = True  # Option : utiliser un dossier temporaire
crop = True     # Option : recadrer les images
uniform_size = False  # Option : uniformiser la taille des images

# Étape 1 : Préparer les images (avec ou sans tmp et crop)
prepared_folders = prepare_images(input_folders, tmp_folder, use_tmp, crop, uniform_size, 8)

# Étape 2 : Créer le spritesheet à partir des images préparées
create_spritesheet(prepared_folders, output_path)
