import React from "react";

function FrameList({ frames, currentFrameIndex, setCurrentFrameIndex, setFrameProperties }) {
  return (
    <div className="frame-list">
      <h3>Frames</h3> {/* Ajout du libellé */}
      <ul>
        {frames.map((frame, index) => (
          <li
            key={index}
            onClick={() => {
              setCurrentFrameIndex(index); 
              setFrameProperties({
                x: frame.x,
                y: frame.y,
              });
            }}
            className={index === currentFrameIndex ? "active" : ""}
          >
            {frame.file}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default FrameList;
