import React from "react";

function StateSelect({ states, selectedState, setSelectedState }) {
  return (
    <div>
      <label>State:</label>
      <select onChange={(e) => setSelectedState(e.target.value)} value={selectedState}>
        <option value="">Select a State</option>
        {states.map((state) => (
          <option key={state} value={state}>
            {state}
          </option>
        ))}
      </select>
    </div>
  );
}

export default StateSelect;
