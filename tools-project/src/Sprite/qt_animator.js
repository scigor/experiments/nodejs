const express = require('express');
const fs = require('fs');
const path = require('path');
const app = express();

const cors = require('cors');
app.use(cors());

// Répertoire de base
const BASE_DIR = path.join(__dirname, 'Assets', 'project1', 'Assets', 'Sprites');

// Route pour obtenir la liste des acteurs
app.get('/api/actors', (req, res) => {
    try {
        const actors = fs.readdirSync(BASE_DIR).filter(file => {
            const fullPath = path.join(BASE_DIR, file);
            return fs.lstatSync(fullPath).isDirectory();
        });
        res.json(actors);
    } catch (error) {
        console.error("Erreur lors de la récupération des acteurs :", error);
        res.status(500).json({ error: 'Erreur serveur lors de la récupération des acteurs.' });
    }
});

// Route pour obtenir les états d'un acteur spécifique
app.get('/api/states/:actor', (req, res) => {
    const actor = req.params.actor;
    const actorPath = path.join(BASE_DIR, actor, 'Sprites');

    try {
        const states = fs.readdirSync(actorPath).filter(file => {
            const fullPath = path.join(actorPath, file);
            return fs.lstatSync(fullPath).isDirectory();
        });
        res.json(states);
    } catch (error) {
        console.error(`Erreur lors de la récupération des états pour l'acteur ${actor} :`, error);
        res.status(404).json({ error: `Impossible de trouver les états pour l'acteur ${actor}.` });
    }
});

// Route pour obtenir les fichiers d'un état spécifique d'un acteur
app.get('/api/files/:actor/:state', (req, res) => {
    const { actor, state } = req.params;
    const statePath = path.join(BASE_DIR, actor, 'Sprites', state);

    try {
        const files = fs.readdirSync(statePath).filter(file => /\.(png|jpg|jpeg)$/i.test(file));
        res.json(files);
    } catch (error) {
        console.error(`Erreur lors de la récupération des fichiers pour l'état ${state} de l'acteur ${actor} :`, error);
        res.status(404).json({ error: `Impossible de trouver les fichiers pour l'état ${state} de l'acteur ${actor}.` });
    }
});

// Route pour obtenir les données des frames d'un état spécifique d'un acteur
app.get('/api/frames/:actor/:state', (req, res) => {
    const { actor, state } = req.params;
    const frameDataPath = path.join(BASE_DIR, actor, 'Sprites', state, 'frame_data.json');

    try {
        if (fs.existsSync(frameDataPath)) {
            const frameData = fs.readFileSync(frameDataPath, 'utf8');
            res.json(JSON.parse(frameData));
        } else {
            // Si le fichier n'existe pas, on génère des données dynamiques
            const statePath = path.join(BASE_DIR, actor, 'Sprites', state);
            const files = fs.readdirSync(statePath).filter(file => /\.(png|jpg|jpeg)$/i.test(file));

            const dynamicFrames = files.map((file, index) => ({
                x: index * 0, // Exemple de position dynamique
                y: index * 0,
                scale: 1 + index * 0.1,
                file: file
            }));
            res.json(dynamicFrames);
        }
    } catch (error) {
        console.error(`Erreur lors de la récupération des frames pour l'état ${state} de l'acteur ${actor} :`, error);
        res.status(404).json({ error: `Impossible de trouver les frames pour l'état ${state} de l'acteur ${actor}.` });
    }
});

// Route pour servir l'image demandée
app.get('/api/frames/:actor/:state/:file', (req, res) => {
    const { actor, state, file } = req.params;
    const filePath = path.join(BASE_DIR, actor, 'Sprites', state, file);

    // Vérifier si le fichier existe
    res.sendFile(filePath, (err) => {
        if (err) {
            console.log("Erreur lors de l'envoi du fichier:", err);
            res.status(404).send('Image non trouvée');
        }
    });
});

// Lancer le serveur
app.listen(3001, () => {
    console.log('API running on http://localhost:3001');
});
