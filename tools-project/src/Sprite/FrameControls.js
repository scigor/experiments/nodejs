import React from "react";

function FrameControls({
  frameProperties,
  setFrameProperties,
  updateFrameProperties,
  saveProperties,
}) {
  return (
    <div className="properties">
      <div>
        <label>X Offset:</label>
        <input
          type="number"
          value={frameProperties.x}
          onChange={(e) => {
            setFrameProperties({ ...frameProperties, x: parseInt(e.target.value, 10) });
            updateFrameProperties({ x: parseInt(e.target.value, 10) });
          }}
        />
      </div>
      <div>
        <label>Y Offset:</label>
        <input
          type="number"
          value={frameProperties.y}
          onChange={(e) => {
            setFrameProperties({ ...frameProperties, y: parseInt(e.target.value, 10) });
            updateFrameProperties({ y: parseInt(e.target.value, 10) });
          }}
        />
      </div>
      <button onClick={saveProperties}>Save Properties</button>
    </div>
  );
}

export default FrameControls;
