// Rendu.js
import React, { useState, useEffect  } from 'react';

const ImageSliderRendu = ({ goToPrevious, goToNext, slides, currentIndex, handleCpyPosPromptChange }) => {
    //const [sliderPosPrompt, setSliderPosPrompt] = useState(slides[currentIndex].positive_prompt);
    const [sliderPosPrompt, setSliderPosPrompt] = useState('');

    // Vérifiez si slides et currentIndex sont valides
    useEffect(() => {
        if (slides && slides[currentIndex]) {
            setSliderPosPrompt(slides[currentIndex].positive_prompt || '');
        }
    }, [currentIndex, slides]);

    // Gestion des erreurs si slides[currentIndex] est undefined
    if (!slides || slides.length === 0 || !slides[currentIndex]) {
        return <div>Chargement des images...</div>;  // Message de chargement ou erreur si données invalides
    }

    return (
        <div className="sliderContainer">
          <button onClick={goToPrevious} className="arrowButton left">←</button>
          <div className="imageContainer">
            <img 
              src={slides[currentIndex].image} 
              alt={slides[currentIndex].positive_prompt}
              className="image"
            />
          </div>
          <button onClick={goToNext} className="arrowButton right">→</button>

	  {/* Prompt aligné en dessous de l'image */}
          <div className="promptContainer">
	      <label>Pos Prompt:</label>
              <textarea
                value={sliderPosPrompt}
	        onChange={(e) => setSliderPosPrompt(e.target.value)}
                readOnly
                rows="4"
	        className="promptTextarea"
              />
          </div>
          <button onClick={() => handleCpyPosPromptChange(sliderPosPrompt)}>
            Copier vers le formulaire
          </button>
        </div>
    );
};

export default ImageSliderRendu;

