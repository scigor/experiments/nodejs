// FormRendu.js
import React, { useState }  from 'react';

const FormRendu = ({ 
	filteredModels,
	filteredLoras,
	handleModelChange,
	selectedModel,
	handleLoraChange,
	selectedLora,
	posPrompt,
	negPrompt,
	onPosChange,
	onNegChange,
	output,
	handleSubmit
}) => {

    // Ajout d'un état pour contrôler la visibilité de la liste Lora
    const [isLoraEnabled, setIsLoraEnabled] = useState(false);

    // Fonction pour basculer l'état du checkbox
    const handleCheckboxChange = () => {
        setIsLoraEnabled(prevState => !prevState);
    };

    // État pour les tags
    const [tags, setTags] = useState([]);

    // Fonction pour appeler le backend Flask
    const handleFetchTags = async () => {
        try {
            const response = await fetch('http://212.47.250.95:5000/api/tags', { 
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ prompt: posPrompt }), // Envoyer le contenu du posPrompt
            });
            const data = await response.json(); // Attendre la réponse en JSON
            setTags(prevTags => [...prevTags, ...data.tags]); // Ajouter les nouveaux tags aux anciens
        } catch (error) {
            console.error('Erreur lors de la récupération des tags:', error);
        }
    };

    // Fonction pour gérer la pression longue
    const handleLongPress = () => {
        console.log("Long press detected. You could add more tags.");
        // Ici, vous pouvez décider d'ajouter des tags d'une manière ou d'une autre
        // Par exemple, appeler `handleFetchTags` à nouveau pour ajouter plus de tags
    };

    return (
        <div className="rendu-container">
          {/* Section Model */}
          <div>
            <label htmlFor="model">Model:</label>
            <select id="model" onChange={handleModelChange} value={selectedModel}>
                {filteredModels.map((model, index) => (
                  <option key={index} value={model}>{model}</option>
                ))}
            </select>
          </div>
          {/* Section Lora avec checkbox et label */}
          <div>
            <label>
	      <input 
                type="checkbox"
	        checked={isLoraEnabled}
	        onChange={handleCheckboxChange}
	      />
	      Lora:
	    </label>
	    {isLoraEnabled && (
              <select id="lora" onChange={handleLoraChange} value={selectedLora}>
                {filteredLoras.map((lora, index) => (
                   <option key={index} value={lora}>{lora}</option>
                ))}
              </select>
	    )}
          </div>
          <div>
            <label htmlFor="pos_input">Pos Prompt:</label>
            <textarea
              id="pos_input"
              value={posPrompt}
              onChange={onPosChange}
              rows="4"
              style={{ width: '100%' }}
            />
          </div>
	  <button
	    onClick={handleFetchTags}
	    onMouseDown={() => setTimeout(handleLongPress, 1000)}
            onMouseUp={() => clearTimeout(handleLongPress)}
	  >
	    Obtenir des tags
	  </button>  
	  <div className="tagsContainer">
            {tags.map((tag, index) => (
                 <button key={index} className="tagButton">
		    {tag}
		 </button>
            ))}
          </div>
          <div>
            <label htmlFor="neg_input">Neg Prompt:</label>
            <textarea
              id="neg_input"
              value={negPrompt}
              onChange={onNegChange}
              rows="4"
              style={{ width: '100%' }}
            />
          </div>
          <div>
	    <form onSubmit={handleSubmit}>
              <label>Output:</label>
              <textarea
                value={output}
                readOnly
                rows="4"
                style={{ width: '100%', backgroundColor: '#f0f0f0' }}
              />
            <button type="submit">Submit</button>
           </form>
	</div>
      </div>
    );
};

export default FormRendu;

