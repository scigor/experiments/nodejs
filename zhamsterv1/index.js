const express = require('express');
const routes = require('./routes/sample')
const mqtt = require('./controllers/mqtt')
const { MongoClient } = require('mongodb');

const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);
const dbName = 'myProject';

const app = express();

const path = require('path');

async function main() {
   // Use connect method to connect to the server
   await client.connect();
   console.log('Connected successfully to server');
   const db = client.db(dbName);
   const collection = db.collection('documents');

   // the following code examples can be pasted here...

   return 'done.';
}

main()
.then(console.log)
.catch(console.error)
.finally(() => client.close());

//app.use(express.json());
app.use(express.static('public'));

app.get('/', function (req, res) {
  //res.send('Hello World');
  res.sendFile(path.join(__dirname,"index.html"));
})

app.use('/sample', routes);

const server = app.listen(5000, function () {
  console.log("Express App running at http://127.0.0.1:5000/");
})

/*
const http = require('http');

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Hello World!');
}).listen(8080);
*/
