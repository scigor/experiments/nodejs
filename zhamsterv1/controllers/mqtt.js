const mqtt = require('mqtt');

const TOPIC = '#';
const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 1883;
const USERNAME = process.env.USERNAME || 'username';
const PASSWORD = process.env.PASSWORD || 'password';
const PROTOCOL = 'mqtt';

const clientOption = {
  port: PORT,
  host: HOST,
  //username: USERNAME,
  //password: PASSWORD,
  protocol: PROTOCOL
};

const client  = mqtt.connect(clientOption);
//console.log(`Connecting to mqtt://${HOST}:${PORT}@${USERNAME} topic:${TOPIC} ...`);
console.log(`Connecting to mqtt://${HOST}:${PORT} topic:${TOPIC} ...`);

client.on('connect', function () {
  console.log(`Connected!`);

  client.subscribe(TOPIC, function(err) {
    if(err) {
      console.error(err);
    } else {
      console.log(`Subscription to ${TOPIC} successful.`);
    }
  });

  client.on('message', function (topic, message, packet) {
    // message is Buffer
    console.log(`Incoming message to topic = ${topic} ...`);
    console.log(message.toString());

    console.log('Packet');
    console.log(packet);

    console.log('packet.payload');
    console.log(packet.payload.toString());

    console.log('JSON parse message');
    console.log(JSON.parse(message));

    console.log('Preparing outbound message');
    const outboundMsg = {...message, source: topic}

    console.log('Outbound message below');
    console.log(outboundMsg);
  });

});

module.exports = mqtt;
