const express = require('express');
const router  = express.Router(); 

const sampleController = require('../controllers/sample'); 

const bodyParser = require('body-parser');

// Create application/x-www-form-urlencoded parser
const urlencodedParser = bodyParser.urlencoded({ extended: false })

router.get('/process_get', sampleController.process_get); 

router.post('/process_post', urlencodedParser, sampleController.process_post); 

module.exports = router;
